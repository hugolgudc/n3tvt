import shlex
from datetime import timedelta
from os import listdir
from os.path import isfile, join
from threading import Timer

from django.shortcuts import render_to_response
from django.conf import settings
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import IntegrityError
from django.db.models import Q, Count, Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render
from scapy.all import *
from scapy.layers.inet import IP, Ether, ARP

from n3tvt.forms import SignUpForm, UploadForm, CreateProjectForm, EditProjectForm
from n3tvt.models import Project, UploadedFile, PcapPacket, PcapFlow


@login_required()
def index(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/n3tvt/login')
    project = Project.objects.filter(Q(admin__username=request.user.username) |
                                     Q(users__username__exact=request.user.username)).distinct().order_by('id')
    projects_list = {'projects': project}
    return render(request, 'index.html', projects_list)


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/n3tvt/")
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect("/n3tvt")
    else:
        form = AuthenticationForm()
    return render(request, "registration/login.html", {'form': form})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            user = request.POST['username']
            password = request.POST['password1']
            access = authenticate(username=user, password=password)
            if access is not None:
                auth_login(request, access)
                return HttpResponseRedirect("/n3tvt")
    else:
        form = SignUpForm()
    return render(request, "registration/signup.html", {
        'form': form
    })


@login_required()
def create_project(request):
    if request.method == 'POST':
        project = CreateProjectForm(request.POST)
        form = UploadForm(request.POST, request.FILES)
        if project.is_valid() and form.is_valid():
            x = project.save()
            upload = form.save(commit=False)
            upload.related_project = x
            upload.save()
            t_parsing_packets = Timer(0, parsing_pcap_file, (project, upload,))
            t_parsing_packets.start()
            # t_launch_collector = Timer(10, launch_collector, (upload,))
            # t_launch_collector.start()
            # t_launch_exporter = Timer(10, launch_exporter, (project, upload,))
            # t_launch_exporter.start()
            return HttpResponseRedirect("/n3tvt")
    else:
        project = CreateProjectForm()
        form = UploadForm()
    return render(request, 'createproject.html', {
        'project': project, 'form': form,
    })


@login_required()
def delete_project(request, project_id):
    project = Project.objects.get(id=project_id)
    if request.method == 'POST':
        if request.user.username == project.admin.username:
            upload_file = UploadedFile.objects.get(related_project=project)
            upload_file.delete()
            for root, dirs, files in os.walk(settings.MEDIA_ROOT):
                for d in dirs:
                    dir = os.path.join(root, d)
                    # check if dir is empty
                    if not os.listdir(dir):
                        os.rmdir(dir)
            project.delete()
        else:
            project.users.remove(request.user)
        return HttpResponseRedirect("/n3tvt")
    return render(request, 'deleteproject.html', {'project': project})


@login_required()
def edit_project(request, project_id):
    project = Project.objects.get(id=project_id)
    if request.user.username == project.admin.username:
        if request.method == 'GET':
            form = EditProjectForm(instance=project)
        else:
            form = EditProjectForm(request.POST, instance=project)
            if form.is_valid():
                form.save()
            return HttpResponseRedirect("/n3tvt")
        return render(request, 'editproject.html', {'form': form, 'project': project})


def parsing_pcap_file(project, upload):
    path_pcap_file = settings.BASE_DIR + upload.document.url
    tshark_command = 'tshark -r ' + path_pcap_file
    args_tshark = shlex.split(tshark_command)
    p_tshark = subprocess.Popen(args_tshark, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, errors = p_tshark.communicate()
    if output:
        packets = output.splitlines()
        for packet in packets:
            preparated_packet = packet.split()
            pcap_packet = PcapPacket(packet_number=preparated_packet[0],
                                     mac_address_src=' '.join(preparated_packet[-3:-2]),
                                     mac_address_dst=' '.join(preparated_packet[-1:]),
                                     ip_address_src=None if ' '.join(preparated_packet[-3:-2]) ==
                                                            preparated_packet[2] else preparated_packet[2],
                                     ip_address_dst=None if ' '.join(preparated_packet[-1:]) ==
                                                            preparated_packet[4] else preparated_packet[4],
                                     packet_length=preparated_packet[6],
                                     packet_protocol=preparated_packet[5],
                                     packet_time=preparated_packet[1],
                                     packet_info=' '.join(preparated_packet[7:-3]),
                                     related_project_id=project.instance.id
                                     )
            pcap_packet.save()

    # pkts = rdpcap(settings.BASE_DIR + upload.document.url)
    # packet_counter = 1
    # table = {num: name[8:] for name, num in vars(socket).items() if name.startswith("IPPROTO")}
    # for pkt in pkts:
    #
    #     if Ether in pkt:
    #         mac_address_src = pkt[Ether].src
    #         mac_address_dst = pkt[Ether].dst
    #         packet_type = pkt[Ether].type
    #     else:
    #         mac_address_src = None
    #         mac_address_dst = None
    #         packet_type = None
    #
    #     if IP in pkt:
    #         ip_address_src = pkt[IP].src
    #         ip_address_dst = pkt[IP].dst
    #         packet_protocol = table[pkt[IP].proto]
    #     elif ARP in pkt and IP not in pkt:
    #         ip_address_src = pkt[ARP].psrc
    #         ip_address_dst = pkt[ARP].pdst
    #         packet_protocol = pkt[ARP].ptype
    #     else:
    #         ip_address_src = None
    #         ip_address_dst = None
    #         packet_protocol = None
    #
    #     pcap_packet = PcapPacket(packet_number=packet_counter,
    #                              mac_address_src=mac_address_src,
    #                              mac_address_dst=mac_address_dst,
    #                              packet_type=packet_type,
    #                              ip_address_src=ip_address_src,
    #                              ip_address_dst=ip_address_dst,
    #                              packet_length=len(pkt),
    #                              packet_protocol=packet_protocol,
    #                              packet_time=pkt.time,
    #                              related_project_id=project.instance.id
    #                              )
    #     pcap_packet.save()
    #     packet_counter += 1


def launch_exporter(project, upload):
    complete_path = settings.BASE_DIR + upload.document.url
    directory = os.path.dirname(complete_path) + '/processed_files'
    exporter_command = 'softflowd -v 5 -n 127.0.0.1:6666 -r ' + complete_path
    args = shlex.split(exporter_command)
    historical_size = -1
    while historical_size != os.path.getsize(complete_path):
        historical_size = os.path.getsize(complete_path)
        time.sleep(1)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
    t_shutdown_collector = Timer(360, shutdown_collector, ())
    t_shutdown_collector.start()
    flow_counter = 1
    end_exporter_time = datetime.now()
    while (datetime.now() - end_exporter_time) <= timedelta(seconds=360):
        onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f))]
        for file in onlyfiles:
            if "current" in file or os.path.getsize(os.path.join(directory, file)) <= 276:
                continue
            else:
                nfdump_command = 'nfdump -r ' + os.path.join(directory, file)
                args_nfdump = shlex.split(nfdump_command)
                p_nfdump = subprocess.Popen(args_nfdump, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                output, errors = p_nfdump.communicate()
                if output:
                    output_without_headers = output.split('\n', 1)[-1]
                    output_split = output_without_headers.split('Summary: ')
                    set_of_flows = output_split[0]
                    summary = output_split[1]
                    flows = set_of_flows.splitlines()
                    for flow in flows:
                        preparated_flow = flow.split()
                        try:
                            if '.' in preparated_flow[8] and 'M' == preparated_flow[9]:
                                num_bytes = int(float(preparated_flow[8])*10**6)
                            else:
                                num_bytes = int(preparated_flow[8])
                            src_ip_addr, src_port = preparated_flow[4].split(':')
                            dst_ip_addr, dst_port = preparated_flow[6].split(':')
                            pcap_flow = PcapFlow(flow_number=flow_counter,
                                                 date_first_seen=preparated_flow[0] + ' ' + preparated_flow[1],
                                                 duration=preparated_flow[2],
                                                 protocol=preparated_flow[3],
                                                 src_ip_addr=src_ip_addr,
                                                 src_port=src_port,
                                                 dst_ip_addr=dst_ip_addr,
                                                 dst_port=dst_port,
                                                 num_packets=preparated_flow[7],
                                                 bytes=num_bytes,
                                                 related_project_id=project.instance.id
                                                 )
                            pcap_flow.save()
                            flow_counter += 1
                        except (IntegrityError, ValidationError) as e:
                            flow_counter -= 1
                            pass
                        except:
                            pass


def launch_collector(upload):
    complete_path = settings.BASE_DIR + upload.document.url
    directory = os.path.dirname(complete_path) + '/processed_files'
    nfcapd_command = 'nfcapd -p 6666 -l ' + directory
    args = shlex.split(nfcapd_command)
    if not os.path.exists(directory):
        os.makedirs(directory)
    # os.system('nfcapd -p 6666 -l ' + directory)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()


def shutdown_collector():
    fuser_command = 'fuser -k 6666/udp'
    args = shlex.split(fuser_command)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
    # os.system('fuser -k 6666/udp')


@login_required()
def detail(request, project_id):
    project = Project.objects.get(id=project_id)
    return render(request, 'detail.html', {'project': project})


@login_required()
def packets(request, project_id):
    packet_list = PcapPacket.objects.filter(related_project_id=project_id)
    paginator = Paginator(packet_list, 25)
    page = request.GET.get('page', 1)
    try:
        pcap_packets = paginator.page(page)
    except PageNotAnInteger:
        pcap_packets = paginator.page(1)
    except EmptyPage:
        pcap_packets = paginator.page(paginator.num_pages)

    index = pcap_packets.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 3 if index >= 3 else 0
    end_index = index + 3 if index <= max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]

    return render(request, 'packets.html', {'packet_list': pcap_packets, 'page_range': page_range})


@login_required()
def flows(request, project_id):
    flow_list = PcapFlow.objects.filter(related_project_id=project_id)

    paginator = Paginator(flow_list, 25)
    page = request.GET.get('page', 1)
    try:
        pcap_flows = paginator.page(page)
    except PageNotAnInteger:
        pcap_flows = paginator.page(1)
    except EmptyPage:
        pcap_flows = paginator.page(paginator.num_pages)

    index = pcap_flows.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 3 if index >= 3 else 0
    end_index = index + 3 if index <= max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]

    return render(request, 'flows.html', {'flow_list': pcap_flows, 'page_range': page_range})


@login_required()
def topology(request, project_id):
    pair_src_ip_and_mac_addr = PcapPacket.objects.exclude(mac_address_src__isnull=True)\
        .exclude(ip_address_src__isnull=True).values_list('mac_address_src', 'ip_address_src')\
        .filter(related_project_id=project_id)
    pair_dst_ip_and_mac_addr = PcapPacket.objects.exclude(mac_address_dst__isnull=True)\
        .exclude(ip_address_dst__isnull=True).values_list('mac_address_dst', 'ip_address_dst')\
        .filter(related_project_id=project_id)

    nodes_edges_dot = ""
    flows_list = PcapFlow.objects.filter(related_project_id=project_id)
    for flow in flows_list:
        nodes_edges_dot += str(flow.src_ip_addr + ' -> ' + flow.dst_ip_addr + '; ')
    nodes_edges_dot = nodes_edges_dot[:-2]
    return render(request, 'topology.html', {'flows_list': flows_list, 'nodes_edges_dot': nodes_edges_dot})


@login_required()
def statistics(request, project_id):
    protocols = []
    count_protocol = []
    flow_protocols = PcapFlow.objects.filter(related_project_id=project_id).values('protocol').\
        annotate(dprotocol=Count('protocol'))
    for flow in flow_protocols:
        protocols.append(flow['protocol'])
        count_protocol.append(flow['dprotocol'])

    hosts = []
    bytes = []
    flow_hosts = PcapFlow.objects.filter(related_project_id=project_id).values('src_ip_addr'). \
        annotate(hbytes=Sum('bytes'))
    for flow in flow_hosts:
        hosts.append(flow['src_ip_addr'])
        bytes.append((flow['hbytes']))

    chartdata = {'x': protocols, 'y': count_protocol}
    charttype = "pieChart"
    chartcontainer = 'piechart_container'

    chartdata2 = {'x': hosts, 'y': bytes}
    charttype2 = "pieChart"
    chartcontainer2 = 'piechart_container2'

    data = {
        'charttype': charttype,
        'chartdata': chartdata,
        'chartcontainer': chartcontainer,
        'extra': {
            'x_is_date': False,
            'x_axis_format': '',
            'tag_script_js': True,
            'jquery_on_ready': False,
        },
        'charttype2': charttype2,
        'chartdata2': chartdata2,
        'chartcontainer2': chartcontainer2,
        'extra2': {
            'x_is_date': False,
            'x_axis_format': '',
            'tag_script_js': True,
            'jquery_on_ready': False,
        }
    }
    return render(request, 'statistics.html', data)


def convert_ether_type(pkt):
    type_field = pkt.get_field('type')
    return type_field.i2s[pkt.type]


def convert_ip_proto(pkt):
    proto_field = pkt.get_field('proto')
    return proto_field.i2s[pkt.proto]


def convert_arp_ptype(pkt):
    ptype_field = pkt.get_field('ptype')
    return ptype_field.i2s[pkt.ptype]
