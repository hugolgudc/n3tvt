# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from .validators import validate_file_extension


# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=250, null=True)
    admin = models.ForeignKey(User, related_name='project_managed', on_delete=models.CASCADE)
    users = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class PcapPacket(models.Model):
    packet_number = models.IntegerField(null=True)
    mac_address_src = models.CharField(max_length=50, null=True)
    mac_address_dst = models.CharField(max_length=50, null=True)
    # packet_type = models.CharField(max_length=20, null=True)
    ip_address_src = models.GenericIPAddressField(null=True)
    ip_address_dst = models.GenericIPAddressField(null=True)
    packet_length = models.IntegerField(null=True)
    packet_protocol = models.CharField(max_length=50, null=True)
    packet_time = models.CharField(max_length=50, null=True)
    packet_info = models.CharField(max_length=250, null=True)
    related_project_id = models.IntegerField(null=False)


class PcapFlow(models.Model):
    flow_number = models.IntegerField(null=True)
    date_first_seen = models.CharField(max_length=50, null=True)
    duration = models.CharField(max_length=20, null=True)
    protocol = models.CharField(max_length=20, null=True)
    src_ip_addr = models.CharField(max_length=50, null=True)
    src_port = models.IntegerField(null=True)
    dst_ip_addr = models.CharField(max_length=50, null=True)
    dst_port = models.IntegerField(null=True)
    num_packets = models.IntegerField(null=True)
    bytes = models.IntegerField(null=True)
    related_project_id = models.IntegerField(null=False)

    class Meta:
        unique_together = ("date_first_seen", "duration", "protocol", "src_ip_addr",
                           "src_port", "dst_ip_addr", "dst_port", "num_packets", "bytes",
                           "related_project_id")


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/username/project_name/<filename>
    return '{0}/{1}/{2}'.format(instance.related_project.admin.username, instance.related_project.name, filename)


class UploadedFile(models.Model):
    document = models.FileField(upload_to=user_directory_path, validators=[validate_file_extension])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    related_project = models.ForeignKey(Project, on_delete=models.CASCADE)


class ProcessedFile(models.Model):
    uploaded_file = models.ForeignKey(UploadedFile, on_delete=models.CASCADE)
