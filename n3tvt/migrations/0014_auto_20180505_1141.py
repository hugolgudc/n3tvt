# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-05-05 09:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('n3tvt', '0013_auto_20180505_1140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pcappacket',
            name='related_project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='n3tvt.Project'),
        ),
    ]
