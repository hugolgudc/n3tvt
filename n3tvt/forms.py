from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from n3tvt.models import UploadedFile, Project


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class UploadForm(forms.ModelForm):
    class Meta:
        model = UploadedFile
        fields = ('document',)


class CreateProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'description', 'admin', 'users')
        labels = {
            'name': 'Name',
            'description': 'Description',
            'admin': 'Project Manager',
            'users': 'Users',
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'admin': forms.Select(attrs={'class': 'form-control'}),
            'users': forms.CheckboxSelectMultiple(),
        }


class EditProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'description', 'users')
        labels = {
            'name': 'Name',
            'description': 'Description',
            'users': 'Users',
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'users': forms.CheckboxSelectMultiple(),
        }
