from django.contrib import admin
from n3tvt.models import Project

# Register your models here.
admin.site.register(Project)
