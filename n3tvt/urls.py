from django.conf.urls import url
from django.contrib.auth import views as auth_views

from n3tvt import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^password_reset/$', auth_views.password_reset,
        name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done,
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete,
        name='password_reset_complete'),
    url(r'^create_project/$', views.create_project, name='createproject'),
    url(r'^delete_project/(?P<project_id>\d+)/$', views.delete_project, name='deleteproject'),
    url(r'^edit_project/(?P<project_id>\d+)/$', views.edit_project, name='editproject'),
    url(r'^project_details/(?P<project_id>\d+)/$', views.detail, name='detail'),
    url(r'^project_details/(?P<project_id>\d+)/packets$', views.packets, name='packets'),
    url(r'^project_details/(?P<project_id>\d+)/flows$', views.flows, name='flows'),
    url(r'^project_details/(?P<project_id>\d+)/topology$', views.topology, name='topology'),
    url(r'^project_details/(?P<project_id>\d+)/statistics$', views.statistics, name='statistics'),
]
